package pl.edu.agh.po.lab01;

public class World {


    public static void main(String[] args)
    {
        System.out.println("Start");
        Direction[] converted=convert(args);

        run(converted);
        System.out.println("Stop");
    }

    public static Direction[] convert(String[] toConvert)
    {
        int newLength=0;
        for(String com:toConvert)
        {
            if(com.equals("f")||com.equals("b")||com.equals("r")||com.equals("l"))
            {
                newLength++;//I check the number of valid commands
            }
        }
        Direction[] converted = new Direction[newLength];

        int i=0;

        for(String com:toConvert)
        {
            if(com.equals("f")||com.equals("b")||com.equals("r")||com.equals("l"))
            {
                converted[i]= Direction.valueOf(com);//I use the value of method, after checking whether oor not, the
                //value is valid, and I add it to the next place in the array
                i++;
            }
        }
        return converted;

    }

    public static void run(Direction[] command)
    {
        for(Direction com:command)
        {
            switch(com)
            {
                case f->System.out.println("Cat goes forward");
                case b->System.out.println("Cat goes backwards");
                case r->System.out.println("Cat goes right");
                case l->System.out.println("Cat goes left");
            }

        }



        //printing the arguments with commmas (after deleting comment signs)
        /*
        for(int i=0;i<commands.length-1;i++)//it probablu can  be done with a foreach loop, but I think it looks/wrks better as a normal loop
        {
            System.out.print(commands[i]+", ");
        }
        System.out.print(commands[commands.length-1]);
        System.out.println();
         */

        //----------------------------------------------------------------------------

        //printing the directions with an array of strings (after deleting comment signs)

        /*
        for(String com:commands)
        {
            switch(com)
            {
                case "f"->System.out.println("Cat goes forward");
                case "b"->System.out.println("Cat goes backwards");
                case "r"->System.out.println("Cat goes right");
                case "l"->System.out.println("Cat goes left");
            }
        }
        */

        //-----------------------------------------------------------------------


    }
}
