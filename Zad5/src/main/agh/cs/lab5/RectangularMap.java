package agh.cs.lab5;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class RectangularMap extends AbstractWorldMap implements IWorldMap
{

    public RectangularMap(int width, int height)
    {
        lowerCorner = new Vector2d(0,0);
        upperCorner = new Vector2d(width,height);
    }

    //implementation
    @Override
    public boolean canMoveTo(Vector2d position)
    {
        return position.precedes(upperCorner) && position.follows(lowerCorner) && !isOccupied(position);
    }

    @Override
    public Optional<Object> objectAt(Vector2d position)
    {
        for(Animal animal : animals)
        {
            if(animal.getPosition().equals(position))
            {
                return Optional.of(animal);
            }
        }
        return Optional.empty();
    }
    //implementation end
}
