package agh.cs.lab5;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import static java.lang.StrictMath.sqrt;

public class GrassField  extends AbstractWorldMap implements IWorldMap
{

    private final List<Grass> bushes = new ArrayList<>();

    public GrassField(int n)
    {
        Random r = new Random();
        lowerCorner = new Vector2d(0,0);
        upperCorner = new  Vector2d((int)sqrt(n*10),(int) (sqrt(n*10)));
        for(int i=0; i<n; i++)
        {
            Vector2d pos = new Vector2d(r.nextInt((int)sqrt(n*10)+1),r.nextInt((int) (sqrt(n*10)+1)));
            if(isOccupied(pos))
            {
                i--;
            }
            else
            {
                bushes.add(new Grass(pos));
            }
        }
    }

    @Override
    public boolean canMoveTo(Vector2d position)
    {
        Optional<Object> object = objectAt(position);
        if(!position.precedes(upperCorner))
        {
            upperCorner = position;
        }
        if(!position.follows(lowerCorner))
        {
            lowerCorner = position;
        }
        return object.isEmpty() || !(object.get() instanceof Animal);
    }



    @Override
    public Optional<Object> objectAt(Vector2d position)
    {
        for(Animal animal : animals)
        {
            if(animal.getPosition().equals(position))
            {
                return Optional.of(animal);
            }
        }
        for(Grass grass: bushes)
        {
            if(grass.getPosition().equals(position))
            {
                return Optional.of(grass);
            }

        }
        return Optional.empty();
    }

    public void setBush(Vector2d position)
    {
        if(!isOccupied(position))
        {
            bushes.add(new Grass(position));
        }
    }

}
