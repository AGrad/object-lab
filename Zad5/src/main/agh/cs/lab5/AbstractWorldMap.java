package agh.cs.lab5;

import java.util.ArrayList;
import java.util.List;

abstract class AbstractWorldMap implements IWorldMap
{
    protected final List<Animal> animals = new ArrayList<>();

    protected Vector2d lowerCorner;
    protected Vector2d upperCorner;

    @Override
    public  boolean place(Animal animal)
    {
        if(canMoveTo(animal.getPosition()))
        {
            animals.add(animal);
            return true;
        }
        else return false;
    }

    @Override
    public void run(List<MoveDirection> directions)
    {
        int a = 0;
        for(MoveDirection dir : directions)
        {
            animals.get(a).move(dir);
            a++;
            if(a==animals.size()) a=0;
        }
    }

    @Override
    public boolean isOccupied(Vector2d position)
    {
        return objectAt(position).isPresent();
    }

    @Override
    public String toString()
    {
        MapVisualiser visual = new MapVisualiser(this);
        return visual.draw(lowerCorner, upperCorner);
    }
}
