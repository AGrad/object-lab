package agh.cs.lab5;

import org.junit.jupiter.api.Test;

import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class GrassFieldTest {

    @Test
    void canMoveTo() {
        GrassField grass = new GrassField(10);
        Animal test = new Animal(grass,new Vector2d(1,1));
        grass.place(test);
        assertTrue(grass.canMoveTo(new Vector2d(1,2)));
        assertFalse(grass.canMoveTo((new Vector2d(1,1))));

    }

    @Test
    void canMoveToNegative()
    {
        GrassField grass = new GrassField(0);
        Animal test = new Animal(grass,new Vector2d(-1,-1));
        grass.place(test);
        assertTrue(grass.canMoveTo(new Vector2d(-1,-2)));
        assertFalse(grass.canMoveTo((new Vector2d(-1,-1))));
    }

    @Test
    void placeNormal() {
        GrassField grass = new GrassField(0);
        Animal test = new Animal(grass, new Vector2d(1,1));
        assertTrue(grass.place(test));
        grass.place(test);
        assertTrue(grass.isOccupied(new Vector2d(1,1)));
    }

    @Test
    void placeFalse()
    {
        GrassField grass = new GrassField(0);
        Animal test = new Animal(grass, new Vector2d(1,1));
        grass.place(test);
        Animal test2 = new Animal(grass, new Vector2d(1,1));
        assertFalse(grass.place(test2));
    }

    @Test
    void placeNegative()
    {
        GrassField grass = new GrassField(0);
        Animal test = new Animal(grass, new Vector2d(-1,-1));
        assertTrue(grass.place(test));
        assertTrue(grass.isOccupied(new Vector2d(-1,-1)));
    }

    @Test
    void runNormal() {
        GrassField grass = new GrassField(10);
        Animal test = new Animal(grass, new Vector2d(1,1));
        Animal test2 = new Animal(grass, new Vector2d(2,1));
        grass.place(test);
        grass.place(test2);
        List<MoveDirection> directions = new LinkedList<>();
        directions.add(MoveDirection.RIGHT);
        directions.add(MoveDirection.RIGHT);
        directions.add(MoveDirection.BACKWARD);
        directions.add(MoveDirection.FORWARD);
        grass.run(directions);
        assertEquals(test.getPosition(),new Vector2d(0,1));
        assertEquals(test2.getPosition(),new Vector2d(3,1));
        assertEquals(test.getOrientation(),MapDirection.EAST);
    }

    @Test
    void isOccupied() {
        GrassField grass = new GrassField(0);
        Animal test = new Animal(grass, new Vector2d(1,1));
        grass.place(test);
        assertTrue(grass.isOccupied(new Vector2d(1,1)));
        assertFalse(grass.isOccupied(new Vector2d(0,0)));
    }

    @Test
    void objectAtAnimal() {
        GrassField grass = new GrassField(0);
        Animal test = new Animal(grass, new Vector2d(1,1));
        grass.place(test);
        assertTrue(grass.objectAt(new Vector2d(1,1)).get() instanceof Animal);
    }

    @Test
    void objectAtGrass() {
        GrassField grass = new GrassField(10);
        grass.setBush(new Vector2d(1,1));
        assertTrue(grass.objectAt(new Vector2d(1,1)).get() instanceof Grass);
    }

    @Test
    void objectAtAnimalAndGrass() {
        GrassField grass = new GrassField(10);
        Animal test = new Animal(grass, new Vector2d(1,1));
        grass.place(test);
        grass.setBush(new Vector2d(1,1));
        assertTrue(grass.objectAt(new Vector2d(1,1)).get() instanceof Animal);
    }
}