package agh.cs.lab5;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AnimalTest {

    @Test
    void moveTestsForward()
    {
        IWorldMap map = new RectangularMap(4, 4);
        Animal pet = new Animal(map);//starting position (2,2)
        pet.move(MoveDirection.FORWARD);
        assertEquals(new Vector2d(2,3),pet.getPosition());

        pet.setOrientation(MapDirection.EAST);
        pet.move(MoveDirection.FORWARD);
        assertEquals(new Vector2d(3,3),pet.getPosition());

        pet.setOrientation(MapDirection.WEST);
        pet.move(MoveDirection.FORWARD);
        assertEquals(new Vector2d(2,3),pet.getPosition());

        pet.setOrientation(MapDirection.SOUTH);
        pet.move(MoveDirection.FORWARD);
        assertEquals(new Vector2d(2,2),pet.getPosition());
    }

    @Test
    void moveTestsBackwards()
    {
        IWorldMap map = new RectangularMap(4, 4);
        Animal pet = new Animal(map);//starting position (2,2)
        pet.move(MoveDirection.BACKWARD);
        assertEquals(new Vector2d(2,1),pet.getPosition());

        pet.setOrientation(MapDirection.EAST);
        pet.move(MoveDirection.BACKWARD);
        assertEquals(new Vector2d(1,1),pet.getPosition());

        pet.setOrientation(MapDirection.WEST);
        pet.move(MoveDirection.BACKWARD);
        assertEquals(new Vector2d(2,1),pet.getPosition());

        pet.setOrientation(MapDirection.SOUTH);
        pet.move(MoveDirection.BACKWARD);
        assertEquals(new Vector2d(2,2),pet.getPosition());
    }

    @Test
    void moveTestsLeft()
    {
        IWorldMap map = new RectangularMap(4, 4);
        Animal pet = new Animal(map);//starting position (2,2)
        pet.move(MoveDirection.LEFT);
        assertEquals(MapDirection.WEST,pet.getOrientation());
        pet.move(MoveDirection.LEFT);
        assertEquals(MapDirection.SOUTH,pet.getOrientation());
        pet.move(MoveDirection.LEFT);
        assertEquals(MapDirection.EAST,pet.getOrientation());
        pet.move(MoveDirection.LEFT);
        assertEquals(MapDirection.NORTH,pet.getOrientation());
    }

    @Test
    void moveTestsRight()
    {
        IWorldMap map = new RectangularMap(4, 4);
        Animal pet = new Animal(map);//starting position (2,2)A
        pet.move(MoveDirection.RIGHT);
        assertEquals(MapDirection.EAST,pet.getOrientation());
        pet.move(MoveDirection.RIGHT);
        assertEquals(MapDirection.SOUTH,pet.getOrientation());
        pet.move(MoveDirection.RIGHT);
        assertEquals(MapDirection.WEST,pet.getOrientation());
        pet.move(MoveDirection.RIGHT);
        assertEquals(MapDirection.NORTH,pet.getOrientation());
    }

    @Test
    void moveBeyondBoundariesMovementTestUpperCorner()
    {
        IWorldMap map = new RectangularMap(4, 4);
        Animal pet = new Animal(map);//starting position (2,2)
        Vector2d upperCorner = new Vector2d(4,4);
        pet.setPosition(upperCorner);
        pet.move(MoveDirection.FORWARD);
        assertEquals(upperCorner,pet.getPosition());
        pet.setOrientation(MapDirection.EAST);
        pet.move(MoveDirection.FORWARD);
        assertEquals(upperCorner,pet.getPosition());
        pet.setOrientation(MapDirection.SOUTH);
        pet.move(MoveDirection.BACKWARD);
        assertEquals(upperCorner,pet.getPosition());

    }

    @Test
    void moveBeyondBoundariesMovementTestLowerCorner()
    {
        IWorldMap map = new RectangularMap(4, 4);
        Animal pet = new Animal(map);//starting position (2,2)
        Vector2d lowerCorner = new Vector2d(0,0);
        pet.setPosition(lowerCorner);
        pet.setOrientation(MapDirection.SOUTH);
        pet.move(MoveDirection.FORWARD);
        assertEquals(lowerCorner,pet.getPosition());
        pet.setOrientation(MapDirection.WEST);
        pet.move(MoveDirection.FORWARD);
        assertEquals(lowerCorner,pet.getPosition());
        pet.setOrientation(MapDirection.NORTH);
        pet.move(MoveDirection.BACKWARD);
        assertEquals(lowerCorner,pet.getPosition());
    }


}