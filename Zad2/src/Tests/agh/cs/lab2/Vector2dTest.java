package agh.cs.lab2;

import org.junit.jupiter.api.Test;

import java.util.Vector;

import static org.junit.jupiter.api.Assertions.*;

class Vector2dTest {

    @Test
    void testToString()
    {
        Vector2d tester = new Vector2d(1,1);//I create vector
        String test_text = "(1,1)";//I create string, that should be a result of the ToStrinf function

        assertEquals(test_text,tester.toString());//I test equality

        tester = new Vector2d(-599,-69);//another test with negative numbers
        test_text = "(-599,-69)";
        assertEquals(test_text,tester.toString());

    }

    @Test
    void precedes()
    {
        Vector2d tester = new Vector2d(0,0);
        Vector2d tester_two = new Vector2d(1,1);//tester precedes tester_two, basic test

        assertTrue(tester.precedes(tester_two));

        tester_two=new Vector2d(0,1);//only the y decides, that tester precedes tester two
        assertTrue(tester.precedes(tester_two));

        tester_two = new Vector2d(0,0);
        assertTrue(tester.precedes(tester_two));//it should preceed the equal vector

        assertTrue(tester.precedes(tester));//it should preceed itself

        tester = new Vector2d(-5000,-4500);//with negative numbers
        assertFalse(tester_two.precedes(tester));//tester two does NOT preceed tester

    }

    @Test
    void follows()
    {
        Vector2d tester = new Vector2d(0,0);
        Vector2d tester_two = new Vector2d(1,1);//tester_two follows tester, basic test
        assertTrue(tester_two.follows(tester));

        tester = new Vector2d(1, 1);
        tester_two = new Vector2d(1,1);// the same vector as tester

        assertTrue(tester.follows(tester_two));//tester should follow vector equal to it

        assertTrue((tester.follows(tester)));//tester should follow itself

        tester = new Vector2d(-1000,-45000);
        tester_two = new Vector2d(-1500,4500);

        assertFalse(tester.follows(tester_two));//tester - two negative components, tester_two one negative and one positive ecomponenet

    }

    @Test
    void upperRight()
    {
        Vector2d tester = new Vector2d(1,1);
        Vector2d tester_two = new Vector2d(0,0);
        Vector2d tester_result = new Vector2d(1,1);
        assertEquals(tester_result,tester.upperRight(tester_two));
        assertEquals(tester_result,tester_two.upperRight(tester));//I check, whether the result changes when I call method from different instances

        tester = new Vector2d(-3,50);
        tester_two = new Vector2d(45,-50);
        tester_result=new Vector2d(45,50);//with some negative numbers
        assertEquals(tester_result,tester.upperRight(tester_two));

    }

    @Test
    void lowerLeft()
    {
        Vector2d tester = new Vector2d(1,1);
        Vector2d tester_two = new Vector2d(0,0);
        Vector2d tester_result = new Vector2d(0,0);
        assertEquals(tester_result,tester.lowerLeft(tester_two));
        assertEquals(tester_result,tester_two.lowerLeft(tester));//I check, whether the result changes when I call method from different instances

        tester = new Vector2d(-3,50);
        tester_two = new Vector2d(45,-50);
        tester_result=new Vector2d(-3,-50);//with some negative numbers
        assertEquals(tester_result,tester.lowerLeft(tester_two));
    }

    @Test
    void add()
    {
        Vector2d tester = new Vector2d(0,0);
        Vector2d tester_two = new Vector2d(0,0);
        Vector2d tester_result = new Vector2d(0,0);
        assertEquals(tester_result,tester.add(tester_two));

        tester=new Vector2d(50,1);
        tester_two = new Vector2d(-50,865);
        tester_result =new Vector2d(0,866);
        assertEquals(tester_result,tester.add(tester_two));

        tester=new Vector2d(1,-1);
        tester_result = new Vector2d(2,-2);
        assertEquals(tester_result,tester.add(tester));//whether or not i can add vector to itself, and with negative numbeers
    }

    @Test
    void subtract()
    {
        Vector2d tester = new Vector2d(0,0);
        Vector2d tester_two = new Vector2d(0,0);
        Vector2d tester_result = new Vector2d(0,0);
        assertEquals(tester_result,tester.subtract(tester_two));

        tester=new Vector2d(50,865);
        tester_two = new Vector2d(-50,866);
        tester_result =new Vector2d(100,-1);
        assertEquals(tester_result,tester.subtract(tester_two));

        tester=new Vector2d(1,-1);
        tester_result = new Vector2d(0,0);
        assertEquals(tester_result,tester.subtract(tester));//whether or not i can subtract vector from itself, and with negative numbeers
    }

    @Test
    void testEquals()
    {
        Vector2d tester = new Vector2d(46,23);
        assertTrue(tester.equals(tester));//equals to itself
        assertFalse(tester.equals("(46,23)"));//does NOT equal to string

        Vector2d tester_two= new Vector2d(46,23);
        assertTrue(tester.equals(tester_two));

        tester_two= new Vector2d(23,23);
        assertFalse(tester_two.equals(tester));
    }

    @Test
    void opposite()
    {
        Vector2d tester = new Vector2d(0,0);
        assertEquals(tester,tester.opposite());//because (0,0_

        Vector2d tester_two = new Vector2d(2,-24);
        tester= new Vector2d(-2,24);
        assertEquals(tester_two,tester.opposite());
    }
}