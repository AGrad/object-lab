package agh.cs.lab3;

public class Animal
{
    private MapDirection orientation;
    private Vector2d position;

    public final Vector2d upperCorner = new Vector2d(4,4);
    public final Vector2d lowerCorner = new Vector2d(0,0);

    private Vector2d help;


    public Animal()
    {
        orientation = MapDirection.NORTH;
        position = new Vector2d(2,2);
    }


    public Vector2d getPosition() {//this function helps me with tests
        return position;
    }

    public MapDirection getOrientation() {// this function helps me with tests
        return orientation;
    }

    public void setOrientation(MapDirection orientation) {
        this.orientation = orientation;
    }

    public void setPosition(Vector2d position)
    {
        if(isPositionCorrect(position))
        {
            this.position = position;
        }

    }

    @Override
    public String toString() {
        return  "Orientation= " + orientation +
                ", Position= " + position;
    }

    public void move(MoveDirection direction)
    {

        switch(direction)
        {
            case LEFT -> orientation = orientation.previous();//previous return the conterclokwise next orientatiion
            case RIGHT -> orientation = orientation.next();//next returns the clockwise next orientation
            case FORWARD ->
            {
                help = position.add(orientation.toUnitVector());//the new position
                if(isPositionCorrect(help))//i check, whether or not the new position is correct (in the boundaries)
                {
                    position = help;
                }

            }

            case BACKWARD->
            {
                help = position.subtract(orientation.toUnitVector());//the new position
                if(isPositionCorrect(help))//i check, whether or not the new position is correct (in the boundaries)
                {
                    position = help;
                }
            }

        }

        
    }

    private boolean isPositionCorrect(Vector2d pos)
    {
        return pos.precedes(upperCorner) && pos.follows(lowerCorner);//if the pos is between (0,0), and (4,4) it returns true (also when it equals (0,0) or (4,4)
    }

}
