package agh.cs.lab3;

import java.util.LinkedList;

public class World
{
    public static void main(String[] args)
    {
        Animal pet = new Animal();
        System.out.println(pet.toString());
        LinkedList<MoveDirection> list = OptionsParser.parse(args);
        for(MoveDirection com : list)
        {
            pet.move(com);
            System.out.println(pet.toString());
        }
    }
}
