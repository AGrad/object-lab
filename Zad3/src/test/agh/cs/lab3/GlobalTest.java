package agh.cs.lab3;

import org.junit.jupiter.api.Test;

import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.*;

class GlobalTest {
    @Test
    void globalTest()
    {
        Animal pet=new Animal();//starting position (2,2), starting orientation NORTH
        String[] commands= {"f","forward","f","left","garbage","backward","b","b","right","r","right","f","f","f","r","f","l","f","f"};
        LinkedList<MoveDirection> list = OptionsParser.parse(commands);
        test(2,3, MapDirection.NORTH,list,pet);
        test(2,4, MapDirection.NORTH,list,pet);//last spot before the wall
        test(2,4,MapDirection.NORTH,list,pet);
        test(2,4, MapDirection.WEST,list,pet);
        test(3,4, MapDirection.WEST,list,pet);
        test(4,4, MapDirection.WEST,list,pet);//last spot before the wall
        test(4,4, MapDirection.WEST,list,pet);
        test(4,4, MapDirection.NORTH,list,pet);
        test(4,4, MapDirection.EAST,list,pet);
        test(4,4, MapDirection.SOUTH,list,pet);
        test(4,3, MapDirection.SOUTH,list,pet);
        test(4,2, MapDirection.SOUTH,list,pet);
        test(4,1, MapDirection.SOUTH,list,pet);
        test(4,1, MapDirection.WEST,list,pet);
        test(3,1, MapDirection.WEST,list,pet);
        test(3,1, MapDirection.SOUTH,list,pet);
        test(3,0, MapDirection.SOUTH,list,pet);
        test(3,0, MapDirection.SOUTH,list,pet);




    }

    private void test(int correct_x, int correct_y, MapDirection correct_orientation,LinkedList<MoveDirection> list, Animal test)
    {
        test.move(list.getFirst());
        list.removeFirst();
        assertEquals(new Vector2d(correct_x,correct_y),test.getPosition());
        assertEquals(correct_orientation,test.getOrientation());
    }
}