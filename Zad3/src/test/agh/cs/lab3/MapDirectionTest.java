package agh.cs.lab3;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MapDirectionTest {

    @Test
    void next()
    {
        //I check all possible values of enum
        MapDirection tester = MapDirection.NORTH;
        assertEquals(MapDirection.EAST,tester.next());

        tester = MapDirection.EAST;
        assertEquals(MapDirection.SOUTH,tester.next());

        tester = MapDirection.SOUTH;
        assertEquals(MapDirection.WEST,tester.next());

        tester=MapDirection.WEST;
        assertEquals(MapDirection.NORTH,tester.next());
    }

    @Test
    void previous()
    {
        //I check all possible values of the enum
        MapDirection tester = MapDirection.NORTH;
        assertEquals(MapDirection.WEST,tester.previous());

        tester = MapDirection.WEST;
        assertEquals(MapDirection.SOUTH,tester.previous());

        tester = MapDirection.SOUTH;
        assertEquals(MapDirection.EAST, tester.previous());

        tester = MapDirection.EAST;
        assertEquals(MapDirection.NORTH, tester.previous());
    }
}