package agh.cs.lab3;

import org.junit.jupiter.api.Test;
import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.*;

class AnimalTest {

    @Test
    void moveTestsForward()
    {
        Animal pet = new Animal();//starting position (2,2)
        pet.move(MoveDirection.FORWARD);
        assertEquals(new Vector2d(2,3),pet.getPosition());

        pet.setOrientation(MapDirection.EAST);
        pet.move(MoveDirection.FORWARD);
        assertEquals(new Vector2d(3,3),pet.getPosition());

        pet.setOrientation(MapDirection.WEST);
        pet.move(MoveDirection.FORWARD);
        assertEquals(new Vector2d(2,3),pet.getPosition());

        pet.setOrientation(MapDirection.SOUTH);
        pet.move(MoveDirection.FORWARD);
        assertEquals(new Vector2d(2,2),pet.getPosition());
    }

    @Test
    void moveTestsBackwards()
    {
        Animal pet = new Animal();//starting position (2,2)
        pet.move(MoveDirection.BACKWARD);
        assertEquals(new Vector2d(2,1),pet.getPosition());

        pet.setOrientation(MapDirection.EAST);
        pet.move(MoveDirection.BACKWARD);
        assertEquals(new Vector2d(1,1),pet.getPosition());

        pet.setOrientation(MapDirection.WEST);
        pet.move(MoveDirection.BACKWARD);
        assertEquals(new Vector2d(2,1),pet.getPosition());

        pet.setOrientation(MapDirection.SOUTH);
        pet.move(MoveDirection.BACKWARD);
        assertEquals(new Vector2d(2,2),pet.getPosition());
    }

    @Test
    void moveTestsLeft()
    {
        Animal pet = new Animal();//starting orientation NORTH
        pet.move(MoveDirection.LEFT);
        assertEquals(MapDirection.WEST,pet.getOrientation());
        pet.move(MoveDirection.LEFT);
        assertEquals(MapDirection.SOUTH,pet.getOrientation());
        pet.move(MoveDirection.LEFT);
        assertEquals(MapDirection.EAST,pet.getOrientation());
        pet.move(MoveDirection.LEFT);
        assertEquals(MapDirection.NORTH,pet.getOrientation());
    }

    @Test
    void moveTestsRight()
    {
        Animal pet = new Animal();//starting orientation NORTH
        pet.move(MoveDirection.RIGHT);
        assertEquals(MapDirection.EAST,pet.getOrientation());
        pet.move(MoveDirection.RIGHT);
        assertEquals(MapDirection.SOUTH,pet.getOrientation());
        pet.move(MoveDirection.RIGHT);
        assertEquals(MapDirection.WEST,pet.getOrientation());
        pet.move(MoveDirection.RIGHT);
        assertEquals(MapDirection.NORTH,pet.getOrientation());
    }

    @Test
    void moveBeyondBoundariesMovementTestUpperCorner()
    {
        Animal pet = new Animal();
        pet.setPosition(pet.upperCorner);
        pet.move(MoveDirection.FORWARD);
        assertEquals(pet.upperCorner,pet.getPosition());
        pet.setOrientation(MapDirection.EAST);
        pet.move(MoveDirection.FORWARD);
        assertEquals(pet.upperCorner,pet.getPosition());
        pet.setOrientation(MapDirection.SOUTH);
        pet.move(MoveDirection.BACKWARD);
        assertEquals(pet.upperCorner,pet.getPosition());

    }

    @Test
    void moveBeyondBoundariesMovementTestLowerCorner()
    {
        Animal pet = new Animal();
        pet.setPosition(pet.lowerCorner);
        pet.setOrientation(MapDirection.SOUTH);
        pet.move(MoveDirection.FORWARD);
        assertEquals(pet.lowerCorner,pet.getPosition());
        pet.setOrientation(MapDirection.WEST);
        pet.move(MoveDirection.FORWARD);
        assertEquals(pet.lowerCorner,pet.getPosition());
        pet.setOrientation(MapDirection.NORTH);
        pet.move(MoveDirection.BACKWARD);
        assertEquals(pet.lowerCorner,pet.getPosition());
    }


}