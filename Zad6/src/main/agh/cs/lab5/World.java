package agh.cs.lab5;

import java.util.List;

public class World
{
    public static void main(String[] args)
    {
        try {
            List<MoveDirection> directions = OptionsParser.parse(args);
            IWorldMap map = new GrassField(10);
            map.place(new Animal(map, new Vector2d(3,4)));
            map.run(directions);
            System.out.print(map.toString());
        }
        catch(IllegalArgumentException ex)
        {
            System.out.print(ex);
            return;
        }
    }
}