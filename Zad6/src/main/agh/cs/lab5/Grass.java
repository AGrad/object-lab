package agh.cs.lab5;

public class Grass
{
    private final Vector2d position;


    public Grass(Vector2d pos)
    {
        position = pos;
    }

    public Vector2d getPosition() {
        return position;
    }

    @Override
    public String toString() {
        return "*";
    }
}
