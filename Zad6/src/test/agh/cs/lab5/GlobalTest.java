package agh.cs.lab5;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.*;

class GlobalTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    @BeforeEach
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @AfterEach
    public void restoreStreams() {
        System.setOut(originalOut);
    }


    @Test
    void globalTestOneAnimal()
    {
        IWorldMap map = new RectangularMap(4,4);
        Animal pet=new Animal(map);//starting position (2,2), starting orientation NORTH
        String[] commands= {"f","forward","f","left","backward","b","b","right","r","right","f","f","f","r","f","l","f","f"};
        LinkedList<MoveDirection> list = OptionsParser.parse(commands);
        test(2,3, MapDirection.NORTH,list,pet);
        test(2,4, MapDirection.NORTH,list,pet);//last spot before the wall
        test(2,4,MapDirection.NORTH,list,pet);
        test(2,4, MapDirection.WEST,list,pet);
        test(3,4, MapDirection.WEST,list,pet);
        test(4,4, MapDirection.WEST,list,pet);//last spot before the wall
        test(4,4, MapDirection.WEST,list,pet);
        test(4,4, MapDirection.NORTH,list,pet);
        test(4,4, MapDirection.EAST,list,pet);
        test(4,4, MapDirection.SOUTH,list,pet);
        test(4,3, MapDirection.SOUTH,list,pet);
        test(4,2, MapDirection.SOUTH,list,pet);
        test(4,1, MapDirection.SOUTH,list,pet);
        test(4,1, MapDirection.WEST,list,pet);
        test(3,1, MapDirection.WEST,list,pet);
        test(3,1, MapDirection.SOUTH,list,pet);
        test(3,0, MapDirection.SOUTH,list,pet);
        test(3,0, MapDirection.SOUTH,list,pet);




    }

    private void test(int correct_x, int correct_y, MapDirection correct_orientation,LinkedList<MoveDirection> list, Animal test)
    {
        test.move(list.getFirst());
        list.removeFirst();
        assertEquals(new Vector2d(correct_x,correct_y),test.getPosition());
        assertEquals(correct_orientation,test.getOrientation());
    }

}