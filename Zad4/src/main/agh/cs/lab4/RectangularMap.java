package agh.cs.lab4;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Vector;

public class RectangularMap implements IWorldMap
{
    private int height;
    private int width;

    private final Vector2d lowerCorner;
    private final Vector2d upperCorner;

    private List<Animal> animals = new ArrayList<>();


    public RectangularMap(int width, int height)
    {
        this.height = height;
        this.width = width;
        lowerCorner = new Vector2d(0,0);
        upperCorner = new Vector2d(width,height);
    }

    //implementation
    public boolean canMoveTo(Vector2d position)
    {
        return position.precedes(upperCorner) && position.follows(lowerCorner) && !isOccupied(position);
    }

    public boolean place(Animal animal)
    {
        if(canMoveTo(animal.getPosition()))
        {
            animals.add(animal);
            return true;
        }
        else
        {
            return false;
        }
    }

    public void run(List<MoveDirection> directions)
    {
        int a = 0;
        for(MoveDirection dir : directions)
        {
            animals.get(a).move(dir);
            a++;
            if(a==animals.size()) a=0;
        }
    }

    public boolean isOccupied(Vector2d position)
    {
        for(Animal animal : animals)
        {
            if(animal.getPosition().equals(position))
            {
                return true;
            }
        }
        return false;
    }

    public Optional<Object> objectAt(Vector2d position)
    {
        for(Animal animal : animals)
        {
            if(animal.getPosition().equals(position))
            {
                return Optional.of(animal);
            }
        }
        return Optional.empty();
    }
    //implementation end


    @Override
    public String toString()
    {
        MapVisualiser visual = new MapVisualiser(this);
        return visual.draw(lowerCorner,upperCorner);
    }
}
