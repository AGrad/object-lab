package agh.cs.lab4;

enum MapDirection
{
    NORTH,
    SOUTH,
    WEST,
    EAST;


    public String toString() {
        return switch(this)
        {
            case EAST-> "Wschód";
            case WEST-> "Zachód";
            case NORTH-> "Północ";
            case SOUTH ->  "Południe";//case SOUTH
        };
    }


    public MapDirection next()
    {
        switch(this)
        {
            case SOUTH: return WEST;
            case WEST: return NORTH;
            case NORTH: return EAST;
            default: return SOUTH; //case EAST
            //the compiler did not let me use case EAST without default, so I did it that way
        }
    }

    public MapDirection previous()
    {
        switch(this) {
            case SOUTH: return EAST;
            case EAST: return NORTH;
            case NORTH: return WEST;
            default: return SOUTH;//case WEST
            //same as in method next
        }
    }

    public Vector2d toUnitVector()
    {
        switch(this)
        {
            case NORTH: return new Vector2d(0,1);
            case SOUTH: return new Vector2d(0,-1);
            case EAST: return new Vector2d(1,0);
            default: return new Vector2d(-1,0);//case WEST
        }
    }
}