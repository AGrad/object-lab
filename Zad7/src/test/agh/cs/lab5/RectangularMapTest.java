package agh.cs.lab5;

import org.junit.jupiter.api.Test;
import org.w3c.dom.css.Rect;

import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class RectangularMapTest {

    @Test
    void canMoveTo() {
        RectangularMap test = new RectangularMap(10,10);
        Animal pet = new Animal(test,new Vector2d(1,1));
        test.place(pet);
        assertTrue(test.canMoveTo(new Vector2d(1,2)));
        assertFalse(test.canMoveTo((new Vector2d(1,1))));

    }

    @Test
    void canNotMoveToNegative()
    {
        RectangularMap test = new RectangularMap(10,10);
        assertFalse(test.canMoveTo(new Vector2d(-1,-2)));
    }

    @Test
    void placeNormal() {
        RectangularMap test = new RectangularMap(10,10);
        Animal pet = new Animal(test,new Vector2d(1,1));

        assertTrue(test.place(pet));
        assertTrue(test.isOccupied(new Vector2d(1,1)));
    }



    @Test
    void runNormal() {
        RectangularMap map = new RectangularMap(10,10);
        Animal test = new Animal(map, new Vector2d(1,1));
        Animal test2 = new Animal(map, new Vector2d(2,1));
        map.place(test);
        map.place(test2);
        List<MoveDirection> directions = new LinkedList<>();
        directions.add(MoveDirection.RIGHT);
        directions.add(MoveDirection.RIGHT);
        directions.add(MoveDirection.BACKWARD);
        directions.add(MoveDirection.FORWARD);
        map.run(directions);
        assertEquals(test.getPosition(),new Vector2d(0,1));
        assertEquals(test2.getPosition(),new Vector2d(3,1));
        assertEquals(test.getOrientation(),MapDirection.EAST);
    }

    @Test
    void isOccupied() {
        RectangularMap map = new RectangularMap(10,10);
        Animal test = new Animal(map, new Vector2d(1,1));
        map.place(test);
        assertTrue(map.isOccupied(new Vector2d(1,1)));
        assertFalse(map.isOccupied(new Vector2d(0,0)));
    }

    @Test
    void objectAtAnimal() {
        RectangularMap map = new RectangularMap(10,10);
        Animal test = new Animal(map, new Vector2d(1,1));
        map.place(test);
        assertTrue(map.objectAt(new Vector2d(1,1)).get() instanceof Animal);
    }
}