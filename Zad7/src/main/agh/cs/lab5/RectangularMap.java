package agh.cs.lab5;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class RectangularMap extends AbstractWorldMap implements IWorldMap
{

    public RectangularMap(int width, int height)
    {

        lowerCorner = new Vector2d(0,0);
        upperCorner = new Vector2d(width,height);
        bound = new MapBoundary(lowerCorner,upperCorner);
    }

    //implementation
    @Override
    public boolean canMoveTo(Vector2d position)
    {
        return position.precedes(upperCorner) && position.follows(lowerCorner) && !isOccupied(position);
    }

    @Override
    public Optional<Object> objectAt(Vector2d position)
    {
        if(animals.get(position) != null)
        {
            return Optional.of(animals.get(position));
        }
        return Optional.empty();
    }
    //implementation end
}
