package agh.cs.lab5;

import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.min;

public class MapBoundary implements  IPositionChangeObserver
{
    private Vector2d initialGrassPosRightUpper;
    private Vector2d initialGrassPosLeftLower;

    private final List<Animal> xSort= new ArrayList<Animal>();
    private final List<Animal> ySort= new ArrayList<Animal>();

    public MapBoundary()
    {
        initialGrassPosLeftLower = new Vector2d(0,0);
        initialGrassPosRightUpper = new Vector2d(0,0);

    }
    public MapBoundary(Vector2d grassPosLeft,Vector2d grassPosRight)
    {
        initialGrassPosRightUpper = grassPosRight;
        initialGrassPosLeftLower = grassPosLeft;
    }

    public void addAnimal(Animal animal)
    {
        if(xSort.size() == 0)
        {
            xSort.add(animal);
            ySort.add(animal);
            return;
        }
        int i = 0;
        while(animal.getPosition().x<xSort.get(i).getPosition().x && i<xSort.size())
        {
            i++;
        }
        while(animal.getPosition().x==xSort.get(i).getPosition().x
                && animal.getPosition().y<xSort.get(i).getPosition().y
                && i<xSort.size());

        {
            i++;
        }
        xSort.add(i,animal);

        i = 0;
        while(animal.getPosition().y<ySort.get(i).getPosition().y && i<ySort.size())
        {
            i++;
        }
        while(animal.getPosition().y==ySort.get(i).getPosition().y
                && animal.getPosition().x<ySort.get(i).getPosition().x
                && i<ySort.size());
        {
            i++;
        }
        ySort.add(i,animal);
    }

    @Override
    public void positionChanged(Vector2d oldPosition, Vector2d newPosition)
    {
        int i =0;
        while(!xSort.get(i).getPosition().equals(newPosition))
        {
            i++;
        }
        if(i != xSort.size() - 1) {
            Animal tmp = xSort.get(i);
            xSort.remove(tmp);
            ySort.remove(tmp);
            addAnimal(tmp);
            return;
        }
        i=0;
        while(!ySort.get(i).getPosition().equals(newPosition))
        {
            i++;
        }
        if(i != ySort.size() - 1) {
            Animal tmp = ySort.get(i);
            xSort.remove(tmp);
            ySort.remove(tmp);
            addAnimal(tmp);
            return;
        }
    }

    public Vector2d upperCorner()
    {
        int x = Math.max(xSort.get(xSort.size()-1).getPosition().x,initialGrassPosRightUpper.x);
        int y = Math.max(ySort.get(ySort.size()-1).getPosition().y,initialGrassPosRightUpper.y);
        return new Vector2d(x,y);
    }

    public Vector2d lowerCorner()
    {
        int x =Math.min(xSort.get(0).getPosition().x,initialGrassPosLeftLower.x);
        int y = Math.min(ySort.get(0).getPosition().y,initialGrassPosLeftLower.y);
        return new Vector2d(x,y);
    }

}
