package agh.cs.lab5;

import java.util.ArrayList;
import java.util.List;

public class Animal implements IPositionChangedPublisher
{
    private MapDirection orientation;
    private Vector2d position;
    private IWorldMap map;
    private final List<IPositionChangeObserver> observers = new ArrayList<IPositionChangeObserver>();

    public Animal(IWorldMap map)
    {
        orientation = MapDirection.NORTH;
        position = new Vector2d(2,2);
        this.map = map;
        addObserver((IPositionChangeObserver) map);
    }

    public Animal(IWorldMap map, Vector2d initialPosition)
    {
        orientation = MapDirection.NORTH;
        position = initialPosition;
        this.map = map;
        addObserver((IPositionChangeObserver) map);
    }


    public Vector2d getPosition() {//this function helps me with tests
        return position;
    }

    public MapDirection getOrientation() {// this function helps me with tests
        return orientation;
    }

    public void setOrientation(MapDirection orientation) {
        this.orientation = orientation;
    }

    public void setPosition(Vector2d position)
    {
        if(map.canMoveTo(position))
        {
            this.position = position;
        }

    }

    @Override
    public String toString() {
       return switch(orientation)
        {
            case EAST ->  ">";
            case WEST-> "<";
            case NORTH->  "^";
            case SOUTH-> "v";
        };
    }

    public void move(MoveDirection direction)
    {
        Vector2d help;

        switch(direction)
        {
            case LEFT -> orientation = orientation.previous();//previous return the conterclokwise next orientatiion
            case RIGHT -> orientation = orientation.next();//next returns the clockwise next orientation
            case FORWARD ->
            {
                help = position.add(orientation.toUnitVector());//the new position
                if(map.canMoveTo(help))//i check, whether or not the new position is correct (in the boundaries)
                {
                    positionChanged(position,help);
                    position = help;
                }

            }

            case BACKWARD->
            {
                help = position.subtract(orientation.toUnitVector());//the new position
                if(map.canMoveTo(help))//i check, whether or not the new position is correct (in the boundaries)
                {
                    positionChanged(position,help);
                    position = help;
                }
            }

        }

        
    }
    @Override
    public void addObserver(IPositionChangeObserver observer)
    {
        observers.add(observer);
    }

    @Override
    public void removeObserver(IPositionChangeObserver observer)
    {
        observers.remove(observer);
    }

    private void positionChanged(Vector2d oldPosition, Vector2d newPosition)
    {
        for(IPositionChangeObserver obs:observers)
        {
            obs.positionChanged(oldPosition, newPosition);
        }
    }


}
