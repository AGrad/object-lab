package agh.cs.lab5;

import java.util.*;

abstract class AbstractWorldMap implements IWorldMap, IPositionChangeObserver
{
    protected Map<Vector2d,Animal> animals = new LinkedHashMap<>();

    protected Vector2d lowerCorner;
    protected Vector2d upperCorner;

    public MapBoundary bound;



    @Override
    public  boolean place(Animal animal)
    {
            if (canMoveTo(animal.getPosition())) {
                animals.put(animal.getPosition(), animal);
                bound.addAnimal(animal);
                return true;
            } else
            {
                throw new IllegalArgumentException(animal.getPosition().toString()+ " jest zajete");
            }
    }

    @Override
    public void run(List<MoveDirection> directions)
    {
        int a =0;
        Collection<Animal> col = animals.values();
        Animal[] array = col.toArray(new Animal[0]);
        int size = array.length;
        for(MoveDirection dir: directions)
        {
            Vector2d position = array[a%size].getPosition();
            System.out.print("x");
            array[a%size].move(dir);
            a+=1;
        }
    }

    @Override
    public boolean isOccupied(Vector2d position)
    {
        return objectAt(position).isPresent();
    }

    @Override
    public String toString()
    {
        MapVisualiser visual = new MapVisualiser(this);
        return visual.draw(bound.lowerCorner(), bound.upperCorner());
    }

    @Override
    public void positionChanged(Vector2d oldPosition, Vector2d newPosition)
    {
        Animal tmp = animals.get(oldPosition);
        animals.remove(oldPosition);
        animals.put(newPosition,  tmp);

    }
}
