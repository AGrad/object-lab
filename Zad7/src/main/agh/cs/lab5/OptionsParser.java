package agh.cs.lab5;

import java.util.LinkedList;

public class OptionsParser
{
    public static LinkedList<MoveDirection> parse(String[] args)
    {
        LinkedList<MoveDirection> list = new LinkedList<MoveDirection>();
        for(String com : args)
        {
                switch (com) {
                    case "f", "forward" -> list.add(MoveDirection.FORWARD);

                    case "b", "backward" -> list.add(MoveDirection.BACKWARD);

                    case "l", "left" -> list.add(MoveDirection.LEFT);

                    case "r", "right" -> list.add(MoveDirection.RIGHT);

                    default -> throw new IllegalArgumentException(com + " is not legal move specification");
                }
        }

        return list;
    }
}
